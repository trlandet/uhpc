from __future__ import division
from collections import deque
from . import methods


def get_neighbours(domain):
    """
    Interpolation methods need a set of neighbours for each dof except those on
    the external boundary. We return a list of neighbour dof numbers for each
    dof in the domain (None for boundary dofs). We allow each dof to have 
    multiple sets of neighbours, so the return value is a list of lists of
    lists of dofs. The outer list is indexed by dof number, the second contains
    one or more sets of dofs and the innermost list contains the dof indices
    themselves.
    """
    Nd = len(domain.dof_coordinates)
    Nd_nonghost =   domain.ghost_offset
    dof_neighbours = [None]*Nd_nonghost
    
    if domain.method == methods.HPC:
        # Standard HPC method, needs structured meshes to work as intended
        for dof in range(Nd_nonghost):
            if domain.is_dirichlet_dof[dof]:
                continue
            
            neighbours = []
            for cid in domain.cell_for_dof[dof]:
                cell = domain.cells[cid]
                neighbours.extend(d for d in cell.dofs if d != dof and d not in neighbours)
                if cell.quad_sister is not None:
                    cell = domain.cells[cell.quad_sister]
                    neighbours.extend(d for d in cell.dofs if d != dof and d not in neighbours)
            
            # Add ghost neighbours for external dofs
            if domain.is_external_dof[dof]:
                external_normals = []
                for fid in domain.facet_for_dof[dof]:
                    facet = domain.facets[fid]
                    if facet.external:
                        external_normals.append(facet.normal)
                
                ext_dofs = [dof] + [nb for nb in neighbours if domain.is_external_dof[nb]]
                for ed in ext_dofs:
                    for fn in external_normals:
                        ginfo = domain.ghost_dof_info[ed]
                        if ginfo is None:
                            continue
                        for gdof, gn in ginfo:
                            d = gn[0]*fn[0] + gn[1]*fn[1]
                            if d > 0.9 and gdof not in neighbours:
                                neighbours.append(gdof)
            
            dof_neighbours[dof] = (neighbours,)
    
        # HACK Find neighbours for free surface derivatives calulation
        for dof in range(Nd_nonghost):
            if not domain.is_dirichlet_dof[dof]:
                continue
            
    
    elif domain.method == methods.KNN:
        # Use the k nearest neighbours
        # Distance = euclidian distance 
        from scipy.spatial import cKDTree
        kdt = cKDTree(domain.dof_coordinates)

        k = domain.order
        assert k > 2
        for dof, coord in enumerate(domain.dof_coordinates[:Nd_nonghost]):
            if domain.is_dirichlet_dof[dof]:
                continue
            _dists, neighbours = kdt.query(coord, k+1)
            neighbours = [d for d in neighbours if d != dof]
            dof_neighbours[dof] = (neighbours,)
    
    elif domain.method == methods.BFS:
        # Find the closest neighbours by breadth first seach
        # Distance = number of facets between dofs
        conn = [[] for _ in domain.dof_coordinates]
        for facet in domain.facets:
            d0, d1 = facet.dofs
            conn[d0].append(d1)
            conn[d1].append(d0)
        
        for dof in range(Nd_nonghost):
            info = domain.ghost_dof_info[dof]
            if info is None:
                continue
            
            for gdof, _normal in info:
                conn[dof].append(gdof)
        
        k = domain.order
        for dof in range(Nd):
            # Depth first search to find k neighbours            
            neighbours = []
            queue = deque()
            queue.append(dof)
            complete = False
            while not complete:
                d = queue.popleft()
                for nb in conn[d]:
                    if nb in neighbours or nb == dof:
                        continue
                    neighbours.append(nb)
                    queue.append(nb)
                    if len(neighbours) == k:
                        complete = True
                        break
            dof_neighbours[dof] = (neighbours,)
    
    elif domain.method == methods.LOCAL_INTERPOLATION:
        for cell in domain.cells:
            for dof in cell.dofs:
                if domain.is_external_dof[dof]:
                    continue
                
                neighbours = [dof_i for dof_i in cell.dofs if dof_i != dof]
                if dof_neighbours[dof] is None:
                    dof_neighbours[dof] = []
                dof_neighbours[dof].append(neighbours)
    
    elif domain.method == methods.OVERLAPPING_INTERPOLATION:
        for facet in domain.facets:
            # Boundary conditions handle external facet dofs
            if facet.external:
                continue
            
            for dof in facet.dofs:
                neighbours = []
                for cid in facet.cells:
                    cell = domain.cells[cid]
                    for dof_i in cell.dofs:
                        if dof_i != dof and dof_i not in neighbours:
                            neighbours.append(dof_i)
                dof_neighbours[dof] = (neighbours,)
    
    return dof_neighbours
