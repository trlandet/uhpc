# encoding: utf8
from __future__ import division
from .polynomials import grad_phi


def apply_bcs(domain, A, b, bcs):
    """
    Apply boundary conditions
    """
    dirichlet_bcs = [bc for bc in bcs if bc[0] == 'D']
    neumann_bcs = [bc for bc in bcs if bc[0] in ('Nx', 'Ny')]
    assert len(dirichlet_bcs) + len(neumann_bcs) == len(bcs)
    
    # Dirichlet BCs
    dirichlet_dofs = set()
    for bc_type, dof, value in dirichlet_bcs:
        A[dof,:] = 0
        A[dof,dof] = 1
        b[dof] = value
        
        assert domain.ghost_dof_info[dof] is None
        dirichlet_dofs.add(dof)
    
    # Neumann BCs
    for bc_type, dof, value in neumann_bcs:
        bc_dir = 0 if bc_type == 'Nx' else 1
        ginfo = domain.ghost_dof_info[dof]
        
        if dof in dirichlet_dofs:
            print 'Skipping Neumann BC on dof %d which is also Dirichlet' % dof
            continue
        
        if len(ginfo) == 1:
            gdof, gn = ginfo[0]
        else:
            gdof0, gn0 = ginfo[0]
            gdof1, gn1 = ginfo[1]
            
            if abs(gn0[bc_dir]) > abs(gn1[bc_dir]):
                gdof, gn = gdof0, gn0
            else:
                gdof, gn = gdof1, gn1
        assert abs(gn[bc_dir]) > 0.9
        
        # Update the global matrix
        diffs = grad_phi(domain, dof)
        for nb, coeff in diffs[bc_dir]:
            A[gdof, nb] = coeff
        b[gdof] = value
