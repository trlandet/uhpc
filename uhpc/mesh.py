# encoding: utf-8
"""
An UHPC domain is a description of the mesh geometry plus the 
location and numbering of the degrees of freedom. The domain is
hence tightly coupled to the polynomial approximation degree
(at least for the moment, this may change).
"""
from __future__ import division
import subprocess
import numpy
from . import methods
from .neighbours import get_neighbours


# Chebyshev nodes in interval [0, 1] (not the standard [-1, 1])
CHEBYSHEV_NODES = [(0.5,),
                   (1.4644660940672621e-01, 8.5355339059327373e-01),
                   (6.6987298107780646e-02, 0.5, 9.3301270189221941e-01),
                   (3.8060233744356631e-02, 3.0865828381745508e-01, 6.9134171618254481e-01, 9.6193976625564337e-01),
                   (2.4471741852423234e-02, 2.0610737385376343e-01, 4.9999999999999994e-01, 7.9389262614623646e-01, 9.7552825814757682e-01),
                   (1.7037086855465844e-02, 1.4644660940672621e-01, 3.7059047744873963e-01, 6.2940952255126037e-01, 8.5355339059327373e-01, 9.8296291314453410e-01)]

CHEBYSHEV_NODES = [(1/2,),
                   (1/3, 2/3),
                   (1/4, 2/4, 3/4),]

class UHPCDomain(object):
    def __init__(self):
        self.order = None
        self.geometric_dimension = None
        self.method = None
        self.cells = []
        self.facets = []
        self.vertex_coordinates = []
        self.dof_coordinates = None
        self.is_external_dof = None
        self.is_dirichlet_dof = None
        self.ghost_dof_info = None
        self._cache = {}
        
        # Connectivity
        self.facet_for_dof = None
        self.cell_for_dof = None
    
    def setup(self, order, method):
        """
        Configure degrees of freedom
        """
        assert self.cells
        self._cache = {}
        init_dofs(self, order, method)
        
    def setup_boundaries(self, dirichlet_dofs=()):
        """
        Add ghost dofs to Neumann dofs
        """
        self.is_dirichlet_dof = [False]*len(self.dof_coordinates)
        for dof in dirichlet_dofs:
            self.is_dirichlet_dof[dof] = True
        init_ghosts(self)
    
    def to_fenics(self):
        """
        Return a FEniCS mesh for the domain
        """
        return to_fenics(self)
    
    def dof_neighbours(self):
        """
        Return a list of neighbour dofs for each non ghost dof in the domain
        """
        if not 'dof_neighbours' in self._cache:
            self._cache['dof_neighbours'] = get_neighbours(self)
        return self._cache['dof_neighbours']
    
    @property
    def ghost_offset(self):
        "The number of non-ghost dofs (and the index of the first ghost dof)"
        return len(self.ghost_dof_info)


class UHPCCell(object):
    def __init__(self, domain, index):
        self.domain = domain
        self.index = index
        self.facets = None
        self.vertices = None
        self.dofs = None
        
        # A quad mesh is divided into triangles where two
        # and two triangles are "sisters"
        self.quad_sister = None
    
    @property
    def midpoint(self):
        n0, n1, n2 = self.vertices
        v0 = self.domain.vertex_coordinates[n0]
        v1 = self.domain.vertex_coordinates[n1]
        v2 = self.domain.vertex_coordinates[n2]
        return (v0[0] + v1[0] + v2[0])/3, (v0[1] + v1[1] + v2[1])/3


class UHPCFacet(object):
    def __init__(self, domain, index):
        self.domain = domain
        self.index = index
        self.cells = None
        self.vertices = None
        self.dofs = None
    
    @property
    def external(self):
        "Is this facet on the external boundary"
        return len(self.cells) == 1
    
    @property
    def normal(self):
        "Facet normal pointing out from cell0"
        # Find vertex coordinates
        n0, n1 = self.vertices
        v0 = self.domain.vertex_coordinates[n0]
        v1 = self.domain.vertex_coordinates[n1]
        
        # Compute normal
        n = (v1[1] - v0[1], v0[0] - v1[0])
        L = (n[0]**2 + n[1]**2)**0.5
        n = (n[0]/L, n[1]/L)
        
        # Make sure the normal is pointing out from cell 0
        cell0 = self.domain.cells[self.cells[0]]
        mp_cell = cell0.midpoint
        mp_facet = (v0[0] + v1[0])/2, (v0[1] + v1[1])/2
        d = (mp_facet[0] - mp_cell[0], mp_facet[1] - mp_cell[1])
        if d[0]*n[0] + d[1]*n[1] < 0:
            n = (-n[0], -n[1])
        
        return n
    
    @property
    def diameter(self):
        "Return the diameter of the facet (same as length in 2D)"
        # Find vertex coordinates
        n0, n1 = self.vertices
        v0 = self.domain.vertex_coordinates[n0]
        v1 = self.domain.vertex_coordinates[n1]
        
        return ((v0[0] - v1[0])**2 + (v0[1] - v1[1])**2)**0.5 


def init_dofs(domain, order, method):
    """
    Given a domain with mesh geometry loaded, add dof information to enable UHPC calculations 
    """
    # Check input
    if not method in methods:
        raise NotImplementedError('Unknown UHPC method %s' % method)
    domain.order = order
    domain.method = method
    domain.dof_coordinates = []
    domain.is_external_dof = []
    domain.facet_for_dof = []
    domain.cell_for_dof = []
    
    # All dofs are uniquely described by their spatial coordinate
    dofmap = {}
    def get_dof(coord):
        if coord not in dofmap:
            dofmap[coord] = len(dofmap)
            domain.dof_coordinates.append(coord)
            domain.is_external_dof.append(False)
            domain.cell_for_dof.append([])
            domain.facet_for_dof.append([])
        return dofmap[coord]
    
    # Loop through the hpc cells and place the dofs
    if method in methods.vertex_based:
        if method == methods.HPC and order != 8:
            raise NotImplementedError('Normal HPC method only defined for 8 neighbours')
        
        # Generate dofs that coincide with the mesh vertices 
        for iv, coord in enumerate(domain.vertex_coordinates):
            assert get_dof(coord) == iv
        
        # Add dof information to cells
        for cell in domain.cells:
            cell.dofs = list(cell.vertices)
            for dof in cell.dofs:
                domain.cell_for_dof[dof].append(cell.index)
        
        # Add dof information to facets    
        for facet in domain.facets:
            facet.dofs = list(facet.vertices)
            for dof in facet.dofs:
                domain.facet_for_dof[dof].append(facet.index)
                if facet.external:
                    domain.is_external_dof[dof] = True
    
    else:
        if not 1 <= order <= len(CHEBYSHEV_NODES):
            raise NotImplementedError('UHPC order %d is not supported' % order)

        # Place dofs at Chebyshev nodes on the facets
        for cell in domain.cells:
            cell.dofs = []
            for fid in cell.facets:
                facet = domain.facets[fid]
                facet.dofs = []
                
                v0 = domain.vertex_coordinates[facet.vertices[0]]
                v1 = domain.vertex_coordinates[facet.vertices[1]]
                for fac in CHEBYSHEV_NODES[order-1]:
                    loc = (v0[0]*fac + v1[0]*(1-fac), v0[1]*fac + v1[1]*(1-fac))
                    dof = get_dof(loc)
                    cell.dofs.append(dof)
                    facet.dofs.append(dof)
                    domain.cell_for_dof[dof].append(cell.index)
                    domain.facet_for_dof[dof].append(fid)
                    
                    # Mark dof locations on the external boundary
                    domain.is_external_dof[dof] = facet.external
            
            d = cell.dofs
            cell.dofs = d[::3] + d[1::3] + d[2::3]
            assert len(cell.dofs) == len(d)
        
        for hpc_facet in domain.facets:
            assert len(hpc_facet.dofs) == order


def init_ghosts(domain):
    """
    Add ghost dofs to the mesh. External dofs that are not Neumann need a
    ghost dof  
    """
    # Mark normal dofs as non-ghosts
    Ndof_nonghost = len(domain.dof_coordinates)
    domain.ghost_dof_info = [None]*Ndof_nonghost
    
    if domain.method not in methods.vertex_based:
        # No ghost dofs implemented for these methods
        return
    
    PARALLEL_CUTOFF = 0.7
    for dof in range(Ndof_nonghost):
        if not domain.is_external_dof[dof] or domain.is_dirichlet_dof[dof]:
            continue
        
        facets = []
        for fid in domain.facet_for_dof[dof]:
            facet = domain.facets[fid]
            if facet.external:
                facets.append(facet)
        
        x0, y0 = domain.dof_coordinates[dof]
        n0 = facets[0].normal
        n1 = facets[1].normal
        d0 = facets[0].diameter
        d1 = facets[1].diameter
        
        if n0[0]*n1[0] + n0[1]*n1[1] > PARALLEL_CUTOFF:
            # Only add one ghost dof with average facet length as distance
            # from the original dof in direction of n0 (which is close to n1) 
            d = (d0 + d1)/2
            x1 = x0 + d*n0[0]
            y1 = y0 + d*n0[1]
            gdof1 = len(domain.dof_coordinates)
            domain.dof_coordinates.append((x1, y1))
            # Add info for neighbour search and BC application
            domain.ghost_dof_info[dof] = ((gdof1, n0),)
        
        else:
            # This is a corner, add two ghost dofs
            # 1) dof with normal n0 and distance d1
            x1 = x0 + d1*n0[0]
            y1 = y0 + d1*n0[1]
            gdof1 = len(domain.dof_coordinates)
            domain.dof_coordinates.append((x1, y1))
            # 2) dof with normal n1 and distance d0
            x2 = x0 + d0*n1[0]
            y2 = y0 + d0*n1[1]
            gdof2 = len(domain.dof_coordinates)
            domain.dof_coordinates.append((x2, y2))
            # Add info for neighbour search and BC application
            domain.ghost_dof_info[dof] = ((gdof1, n0), (gdof2, n1))
    
    Nd_ghost = len(domain.dof_coordinates) - Ndof_nonghost
    print 'Added %d ghost dofs to the existing %d dofs' % (Nd_ghost, Ndof_nonghost)


def from_fenics(mesh):
    """
    This function takes a FEniCS mesh and returns a UHPCDomain
    """
    import dolfin
    
    if not mesh.geometry().dim() == 2:
        raise NotImplementedError('UHPC only implemented in 2D')
    
    # Construct the HPCDomain object and fill it with the information needed 
    # to solve the Laplace equation on the domain
    
    domain = UHPCDomain()
    domain.geometric_dimension = 2
    
    vertices = mesh.coordinates()
    for vtx in vertices:
        domain.vertex_coordinates.append(tuple(vtx))
    
    mesh.init(1, 2)
    connectivity_FC = mesh.topology()(1, 2)
    
    Nc = mesh.num_cells()
    Nf = mesh.num_facets()
    domain.cells = [UHPCCell(domain, cid) for cid in range(Nc)]
    domain.facets = [UHPCFacet(domain, fid) for fid in range(Nf)]
    
    # Loop thorugh the mesh and construct HPCCell and HPCFacet objects to
    # populate the HPCDomain
    for cell in dolfin.cells(mesh):
        hpc_cell = domain.cells[cell.index()]
        
        vids = cell.entities(0)
        fids = cell.entities(1)
        hpc_cell.vertices = tuple(vids)
        hpc_cell.facets = tuple(fids)
        
        # Construct facets
        facets = []
        for i, fid in enumerate(fids):
            hpc_facet = domain.facets[fid]
            hpc_facet.cells = tuple(connectivity_FC(fid))
            hpc_facet.vertices = (vids[(i-1)%3], vids[(i+1)%3])
            facets.append(hpc_facet)

    return domain


def make_rectangle(p0, p1, Nx, Ny, structured=True):
    """
    Generate a rectangular domain
    """
    if not structured:
        return make_unstructured_rectangle(p0, p1, Nx, Ny)
    
    assert isinstance(Nx, (int, long)) and isinstance(Ny, (int, long))
    x0, y0 = p0
    x1, y1 = p1
    
    domain = UHPCDomain()
    domain.geometric_dimension = 2
    
    # Make vertices
    xv = numpy.linspace(x0, x1, Nx+1)
    yv = numpy.linspace(y0, y1, Ny+1)
    vertices = []
    for x in xv:
        for y in yv:
            vertices.append((x, y))
    domain.vertex_coordinates = vertices
    
    # Make cells
    for i in range(Nx):
        for j in range(Ny):
            # Cell indices
            i1 = len(domain.cells)
            i2 = i1 +1
            
            # Cell vertex indices
            n1 = j + i*(Ny+1)
            n2 = n1 + 1
            n3 = n1 + Ny + 2
            n4 = n3 - 1
            
            cell1 = UHPCCell(domain, i1)
            cell1.vertices = (n1, n2, n3)
            cell1.facets = []
            cell1.quad_sister = i2
            domain.cells.append(cell1)
            
            cell2 = UHPCCell(domain, i2)
            cell2.vertices = (n1, n3, n4)
            cell2.facets = []
            cell2.quad_sister = i1
            domain.cells.append(cell2)
    
    # Make facets
    init_facets(domain)    

    return domain


def init_facets(domain):
    """
    When vertices and cells have been created this can be used to add facet info 
    """
    found_facets = {}
    for cell in domain.cells:
        M = len(cell.vertices)
        for i in range(M):
            n1 = cell.vertices[i]
            n2 = cell.vertices[(i+1)%M]
            vids = (n1, n2) if n1 < n2 else (n2, n1)
            
            if vids not in found_facets:
                facet = UHPCFacet(domain, len(domain.facets))
                domain.facets.append(facet)
                facet.vertices = vids
                facet.cells = []
                found_facets[vids] = facet
            
            facet = found_facets[vids] 
            
            facet.cells.append(cell.index)
            cell.facets.append(facet.index)


def make_unstructured_rectangle(p0, p1, Nx, Ny):
    """
    Generate an unstructured rectangular domain by use of gmsh
    """
    # Create gmesh input file
    inp = GMSH_INP
    for var, val in (('p0x', p0[0]), ('p1x', p1[0]), ('p0y', p0[1]), ('p1y', p1[1])):
        inp = inp.replace('$%s' % var, repr(val))
    with open('gmsh_make_unstructured_rectangle.geo', 'wt') as f:
        f.write(inp)
    
    # Create unstructured mesh with gmsh
    dx = (p1[0] - p0[0])/Nx
    cmd = ['gmsh', '-string', 'lc = %f;' % dx,
           '-o', 'gmsh_make_unstructured_rectangle.msh',
           '-2', 'gmsh_make_unstructured_rectangle.geo']
    with open('/dev/null', 'w') as devnull:
        try:
            subprocess.call(cmd, stdout=devnull, stderr=devnull)
        except OSError as e:
            raise RuntimeError('Could not run gmsh. Check that is is in your path. Error: %r' % e)
    
    return read_gmsh('gmsh_make_unstructured_rectangle.msh')

GMSH_INP = """//lc = 1e-2;
Point(1) = {$p0x, $p0y, 0, lc};
Point(2) = {$p1x, $p0y, 0, lc};
Point(3) = {$p1x, $p1y, 0, lc};
Point(4) = {$p0x, $p1y, 0, lc};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line Loop(5) = {4,1,2,3};
Plane Surface(6) = {5};
"""


def read_gmsh(filename):
    """
    Read a Gmsh ASCII msh file and return a UHPC domain
    """
    nodes = []
    elements = []
    with open(filename, 'rt') as inp:
        while True:
            line = inp.readline().strip()
            if not line:
                break
            assert line.startswith('$'), line
            
            if line == '$MeshFormat':
                data = inp.readline().split()
                assert data[0] == '2.2'
                assert data[1] == '0'
                assert data[2] == '8'
                assert inp.readline().strip() == '$EndMeshFormat'
            
            elif line == '$Nodes':
                nnodes = int(inp.readline())
                for _ in range(nnodes):
                    wds = inp.readline().split()
                    index = int(wds[0])
                    x, y, _z = float(wds[1]), float(wds[2]),  float(wds[3]) 
                    nodes.append((x, y))
                    assert len(nodes) == index
                assert inp.readline().strip() == '$EndNodes'
            
            elif line == '$Elements':
                nelem = int(inp.readline())
                for _ in range(nelem):
                    wds = inp.readline().split()
                    index = int(wds[0])
                    
                    # Skip non-triangles (lines, points etc)
                    elem_type = int(wds[1])
                    if elem_type != 2:
                        continue
                    
                    ntags = int(wds[2])
                    n1, n2, n3 = int(wds[ntags+3]), int(wds[ntags+4]),  int(wds[ntags+5]) 
                    elements.append((n1-1, n2-1, n3-1))
                assert inp.readline().strip() == '$EndElements'
             
            else:
                raise NotImplementedError('Unsupported Gmsh data: %r' % line)
    
    domain = UHPCDomain()
    domain.geometric_dimension = 2
    domain.vertex_coordinates = nodes
    
    # Make cells
    for i, vids in enumerate(elements):        
        cell = UHPCCell(domain, i)
        n1, n2, n3 = vids
        v1 = domain.vertex_coordinates[n1]
        v2 = domain.vertex_coordinates[n2]
        v3 = domain.vertex_coordinates[n3]
        cell.vertices = (n1, n2, n3)
        cell.facets = []
        domain.cells.append(cell)
    
    # Make facets
    init_facets(domain)
    
    return domain


def to_fenics(domain):
    import dolfin as df
    
    # Create the mesh and open for editing
    mesh = df.Mesh()
    editor = df.MeshEditor()
    editor.open(mesh, 2, 2)
    
    # Add the vertices
    editor.init_vertices(len(domain.vertex_coordinates))
    for i, coord in enumerate(domain.vertex_coordinates):
        editor.add_vertex(i, coord[0], coord[1])
    
    # Add the cells (triangular elements)
    editor.init_cells(len(domain.cells))
    for i, cell in enumerate(domain.cells):
        n0, n1, n2 = cell.vertices
        editor.add_cell(i, n0, n1, n2)
    
    editor.close()
    return mesh
