from __future__ import division
from .polynomials import phi_at_vertex_coordinates


def plot(domain, q=None):
    """
    Plot the solution phi given a domain and a set of polynomial coefficients. 
    If q is none then only the mesh is plotted
    """
    dim = domain.geometric_dimension
    
    if dim == 2:
        _plot_2D(domain, q)
    else:
        raise NotImplementedError('Cannot plot domain in %dD' % dim)


def interactive():
    from matplotlib import pyplot
    pyplot.show()


def _plot_2D(domain, q):
    from matplotlib import pyplot
    
    # Create triangulation of cells
    x = [v[0] for v in domain.vertex_coordinates]
    y = [v[1] for v in domain.vertex_coordinates]
    triangles = [cell.vertices for cell in domain.cells]
    
    if q is None:
        # Plot the mesh
        fig = pyplot.figure()
        ax = fig.add_subplot(111)
        ax.triplot(x, y, triangles)
    
    else:
        # Plot the solution in the vertices (possibly interpolated)
        fig = pyplot.figure()
        ax = fig.add_subplot(111)
        phi = phi_at_vertex_coordinates(domain, q)
        tp = ax.tripcolor(x, y, triangles, phi, edgecolors='k', shading='gouraud')
        pyplot.colorbar(tp)
