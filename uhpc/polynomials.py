# encoding: utf8
from __future__ import division
from math import cos, sin
import numpy
from . import methods


# Harmonic polynomials defined as follows: C x^ex y^ey => (C, ex, ey)
HARMONIC_POLYNOMIALS_2D = ( 
    ((1, 0, 0),),  # 1: 1
    ((1, 1, 0),),  # 2: x
    ((1, 0, 1),),  # 3: y
    ((1, 2, 0), (-1, 0, 2)), # 4: x² - y² 
    ((2, 1, 1),),            # 5: 2xy
    ((1, 3, 0), (-3, 1, 2)), # 6: x³ - 3xy²
    ((3, 2, 1), (-1, 0, 3)), # 7: 3x²y - y³
    ((1, 4, 0), (-6, 2, 2), (1, 0, 4)), # 8: x⁴ - 6x²y² + y⁴
    ((4, 3, 1), (-4, 1, 3)),            # 9: x³y - xy³
    ((1, 5, 0), (-10, 3, 2), (5, 1, 4)), # 10: x⁵ - 10x³y² + 5xy⁴
    ((1, 0, 5), (-10, 2, 3), (5, 4, 1)),  # 11: 5x⁴y - 10x²y³ + y⁵
    ((1, 6, 0), (-1, 0, 6), (-15, 4, 2), (15, 2, 4)), # 12: x⁶ - 15x⁴y² + 15x²y⁴ − y⁶
    ((-20, 3, 3),(6, 1, 5), (6, 5, 1)),               # 13: 6x⁵y - 20x³y³ + 6xy⁵
    ((1, 7, 0), (-21, 5, 2), (-7, 1, 6), (35, 3, 4)), # 14: x⁷ - 21x⁵y² + 35x³y⁴ - 7xy⁶
    ((-1, 0, 7), (-35, 4, 3), (7, 6, 1), (21, 2, 5)), # 15: 7x⁶y - 35x⁴y³ + 21x²y⁵ - y⁷
    ((1, 8, 0), (1, 0, 8), (-28.0, 2, 6), (-28.0, 6, 2), (70.0, 4, 4)), # 16
    ((-56.0, 5, 3), (-8.0, 1, 7), (8.0, 7, 1), (56.0, 3, 5)), #17
    ((1, 9, 0), (-84.0, 3, 6), (-36.0, 7, 2), (9.0, 1, 8), (126.0, 5, 4)), # 18
    ((1, 0, 9), (-84.0, 6, 3), (-36.0, 2, 7), (9.0, 8, 1), (126.0, 4, 5)), # 19
    ((1, 10, 0), (-1.0, 0, 10), (-210.0, 4, 6), (-45.0, 8, 2), (45.0, 2, 8), (210.0, 6, 4)), # 20
    ((-120.0, 3, 7), (-120.0, 7, 3), (10.0, 1, 9), (10.0, 9, 1), (252.0, 5, 5)), # 21
    ((1, 11, 0), (-462.0, 5, 6), (-55.0, 9, 2), (-11.0, 1, 10), (165.0, 3, 8), (330.0, 7, 4)), # 22
    ((-1.0, 0, 11), (-330.0, 4, 7), (-165.0, 8, 3), (11.0, 10, 1), (55.0, 2, 9), (462.0, 6, 5)), # 23
    ((1, 12, 0), (1, 0, 12), (-924.0, 6, 6), (-66.0, 2, 10), (-66.0, 10, 2), (495.0, 4, 8), (495.0, 8, 4)), # 24
    ((-792.0, 5, 7), (-220.0, 9, 3), (-12.0, 1, 11), (12.0, 11, 1), (220.0, 3, 9), (792.0, 7, 5)), # 25
    ((1, 13, 0), (-1716.0, 7, 6), (-286.0, 3, 10), (-78.0, 11, 2), (13.0, 1, 12), (715.0, 9, 4), (1287.0, 5, 8)), # 26
    ((1, 0, 13), (-1716.0, 6, 7), (-286.0, 10, 3), (-78.0, 2, 11), (13.0, 12, 1), (715.0, 4, 9), (1287.0, 8, 5)), # 27
    ((1, 14, 0), (-1.0, 0, 14), (-3003.0, 8, 6), (-1001.0, 4, 10), (-91.0, 12, 2), (91.0, 2, 12), (1001.0, 10, 4), (3003.0, 6, 8)), # 28
    ((-3432.0, 7, 7), (-364.0, 3, 11), (-364.0, 11, 3), (14.0, 1, 13), (14.0, 13, 1), (2002.0, 5, 9), (2002.0, 9, 5)), #29
)


def evaluate(domain, q, coords, cells):
    """
    Evaluate the harmonic polynomials and calculate the 
    potential, phi, in each of the given coordinates. For
    each coordinate the cell to which the coordinate belongs
    must be given, i.e len(coords) == len(cells).
    
    The array q is the polynomial coefficients found from 
    solving a UHPC problem
    """
    N = len(coords)
    assert len(cells) == N
    phi = numpy.zeros(N, float)
    
    # Create cell local interpolations to calculate values outside dof locations
    Nc = len(domain.cells[0].dofs)
    M = numpy.zeros((Nc, Nc), float)
    p = numpy.zeros(Nc, float)
    for ic, cell in enumerate(cells):
        x0, y0 = coords[ic]
        
        for i, dof_i in enumerate(cell.dofs):
            x, y = domain.dof_coordinates[dof_i]
            for j, poly in enumerate(HARMONIC_POLYNOMIALS_2D[:Nc]):
                fij = 0
                for C, ex, ey in poly:
                    fij += C * (x-x0)**ex * (y-y0)**ey
                M[i,j] = fij
            p[i] = q[dof_i]
        b = numpy.linalg.solve(M, p)
        phi[ic] = b[0]
    
    return phi


def phi_at_dof_coordinates(domain, q):
    """
    Calculate phi at dof locations
    """
    return numpy.array(q[:domain.ghost_offset])


def phi_at_vertex_coordinates(domain, q):
    """
    Calculate phi at the vertices
    """
    if domain.method in methods.vertex_based:
        return numpy.array(q[:domain.ghost_offset])
    
    cells = [None]*len(domain.vertex_coordinates)
    for cell in domain.cells:
        for vid in cell.vertices:
            cells[vid] = cell
    return evaluate(domain, q, domain.vertex_coordinates, cells)


def grad_phi(domain, dof):
    """
    Calculate the gradient of phi at a given dof
    """
    dof_neighbours = domain.dof_neighbours()
    neighbour_sets = dof_neighbours[dof]
    assert len(neighbour_sets) == 1
    
    x0, y0 = domain.dof_coordinates[dof]
    
    neighbours = neighbour_sets[0]
    N = len(neighbours)
    
    M = numpy.zeros((N, N), float)
    for i, dof_i in enumerate(neighbours):
        x, y = domain.dof_coordinates[dof_i]
        xr = x - x0
        yr = y - y0
        
        xrr = cos(0.1)*xr - sin(0.1)*yr
        yrr = sin(0.1)*xr + cos(0.1)*yr
        xr, yr = xrr, yrr
        
        for j, poly in enumerate(HARMONIC_POLYNOMIALS_2D[:N]):
            fij = 0
            for C, ex, ey in poly:
                fij += C * xr**ex * yr**ey
            M[i,j] = fij
    
    try:
        C = numpy.linalg.inv(M)
    except:
        debug_local_matrix_errors(domain, dof, neighbours, M)
    
    res = [], []
    for i, dof_i in enumerate(neighbours):
        res[0].append((dof_i, C[1,i]))
        res[1].append((dof_i, C[2,i])) 
    
    return res


def debug_local_matrix_errors(domain, main_dof, neighbours, M):
    numpy.set_printoptions(linewidth=100000)
    cond = numpy.linalg.cond(M)
    print M
    print 'Number of neighbours', len(neighbours), M.shape
    print 'Condition number: %15.5e' % cond
    
    cells = []
    for cell in domain.cells:
        if main_dof in cell.dofs:
            cells.append(cell)
    
    x0, y0 = domain.dof_coordinates[main_dof]
    xn, yn = zip(*[domain.dof_coordinates[dof] for dof in neighbours])
    
    from matplotlib import pyplot
    
    fig = pyplot.figure()
    fig.patch.set_facecolor('white')
    
    pyplot.plot(xn, yn, 'xb', ms=12)
    pyplot.plot(x0, y0, 'or', ms=12)
    
    for i, dof in enumerate(neighbours):
        x, y = domain.dof_coordinates[dof]
        pyplot.text(x, y, '%d' % (i+1,), fontsize=14, horizontalalignment='left', verticalalignment='bottom')
    
    for cell in cells:
        vids = cell.vertices + cell.vertices[:1]
        x, y = zip(*[domain.vertex_coordinates[vid] for vid in vids])
        pyplot.plot(x, y)
    
    pyplot.gca().set_aspect('equal')
    for f in (pyplot.xlim, pyplot.ylim):
        l, h = f()
        d = h-l
        l -= d*0.1
        h += d*0.1
        f(l, h)
    
    #pyplot.title('DOF %d, cells %r' % (main_dof, [c.index for c in cells]))
    pyplot.title('Condition number %15.5e' % cond)
    pyplot.show()

