from __future__ import division
from math import cos, sin
from collections import Counter
import numpy
from . import methods
from .polynomials import HARMONIC_POLYNOMIALS_2D,  debug_local_matrix_errors


def assemble(domain):
    """
    Assemble a global HPC matrix for the given HPCDomain
    """
    N = len(domain.dof_coordinates)
    A = numpy.zeros((N, N), float)
    b = numpy.zeros(N, float)
    
    assert domain.geometric_dimension == 2
    assert domain.method in methods
    
    if domain.geometric_dimension == 2:
        _assemble_2D(domain, A, b)
    
    return A, b


def _assemble_2D(domain, A, b):
    """
    Assemble UHPC matrix on triangles in 2D
    """
    def check_num_polys(N):
        if N > len(HARMONIC_POLYNOMIALS_2D):
            raise NotImplementedError('Harmonic polynomials of order %d not tabulated yet. ' % N +
                                      'Only %d polynomials are tabulated.' % len(HARMONIC_POLYNOMIALS_2D))
    
    # Get dof neighbours
    dof_neighbours = domain.dof_neighbours()
    assert len(dof_neighbours) == domain.ghost_offset
    
    # Get number of unknowns
    Ns = Counter()
    for neighbour_sets in dof_neighbours:
        if neighbour_sets is not None:
            for neighbours in neighbour_sets:
                Ns[len(neighbours)] += 1
    assert len(Ns) > 0
    print 'Number of neighbour dofs:', ', '.join('%d (%d pts)' % (n, Ns[n]) for n in sorted(Ns))
    
    # Verify that we have tabulated enough polynomials
    for N in Ns:
        check_num_polys(N)
    
    condition_numbers = []
    condmax = [0, None]
    condmin = [1e100, None]
    import copy
    def condition_number_debug(*data):
        cond = numpy.linalg.cond(M)
        condition_numbers.append(cond)
        
        if cond > condmax[0]:
            condmax[0] = cond
            condmax[1] = copy.deepcopy(data)
        if cond < condmin[0]:
            condmin[0] = cond
            condmin[1] = copy.deepcopy(data)
        
    M = numpy.zeros((N, N), float)
    w = numpy.ones(N, float)
    for dof, neighbour_sets in enumerate(dof_neighbours):
        # Skip dofs on the boundary
        if neighbour_sets is None:
            continue
        
        x0, y0 = domain.dof_coordinates[dof]
        set_fac = 1.0/len(neighbour_sets)
        
        for neighbours in neighbour_sets:
            N = len(neighbours)
            if N != M.shape[0]:
                M = numpy.zeros((N, N), float)
                w = numpy.ones(N, float)
            
            for i, dof_i in enumerate(neighbours):
                x, y = domain.dof_coordinates[dof_i]
                xr = x - x0
                yr = y - y0
                
                w[i] = (xr**2 + yr**2)**0.5
            
            #wm = w.mean()
            w[:] = 1#abs(w-wm)
            
            for i, dof_i in enumerate(neighbours):
                x, y = domain.dof_coordinates[dof_i]
                xr = x - x0
                yr = y - y0
                
                xrr = cos(0.1)*xr - sin(0.1)*yr
                yrr = sin(0.1)*xr + cos(0.1)*yr
                xr, yr = xrr, yrr
                
                for j, poly in enumerate(HARMONIC_POLYNOMIALS_2D[:N]):
                    fij = 0
                    for C, ex, ey in poly:
                        fij += C * xr**ex * yr**ey
                    M[i,j] = fij*w[i]
            
            try:
                C = numpy.linalg.inv(M)
                condition_number_debug(dof, neighbours, M)
                #condition_number_debug(dof, neighbours, C)
            except:
                debug_local_matrix_errors(domain, dof, neighbours, M)
                raise
            for i, dof_i in enumerate(neighbours):
                A[dof, dof_i] = -C[0,i]*set_fac*w[i]
            
            A[dof,dof] = 1
            
            if dof == 4 and False:
                debug_local_matrix_errors(domain, dof, neighbours, M)
    
    #from matplotlib import pyplot
    #pyplot.hist(condition_numbers, 100)
    print 'Mean cond %15.5e, std cond %15.5e' % (numpy.mean(condition_numbers), numpy.std(condition_numbers))
    print 'Min  cond %15.5e, max cond %15.5e' % (condmin[0], condmax[0])
    #debug_local_matrix_errors(domain, *condmax[1])
    #debug_local_matrix_errors(domain, *condmin[1])
    #pyplot.show()

