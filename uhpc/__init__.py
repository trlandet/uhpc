class Enum(set):
    def __getattr__(self, name):
        if name in self:
            return name
        raise AttributeError

methods = Enum(('OVERLAPPING_INTERPOLATION', 'LOCAL_INTERPOLATION', 'HPC', 'KNN', 'BFS'))
methods.vertex_based = set((methods.HPC, methods.KNN, methods.BFS))

from .mesh import UHPCDomain, UHPCCell, UHPCFacet, from_fenics, make_rectangle
from .assembly import assemble
from .boundary_conditions import apply_bcs
from .polynomials import evaluate, phi_at_dof_coordinates, grad_phi
from .plotting import plot, interactive
